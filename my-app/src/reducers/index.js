export default function (state = 'choose text', action) {
    switch (action.type) {
        case "PAGE_SELECTED":
            return action.payload;
        default:
            return state;
    }
}