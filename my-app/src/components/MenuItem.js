import './style.css'
import React from 'react'
import {connect} from 'react-redux'
import {select} from '../actions/index'
import {urlBase} from './Config'


const MenuItem = props =>
    <button onClick={() => {
        fetch(urlBase + props.page.api)
            .then(response => response.json())
            .then(result => props.select(result.info))
    }}>
        Show {props.page.name}
    </button>;

export default connect(null, {select})(MenuItem)
