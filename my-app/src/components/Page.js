import React from 'react'
import './style.css'
import {connect} from 'react-redux'


const Page = props =>
    <div className='page'>
        <h1>{props.text}</h1>
    </div>;

export default connect(text => ({text}))(Page)
