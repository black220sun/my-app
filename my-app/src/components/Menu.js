import React from 'react'
import './style.css'
import MenuItem from './MenuItem'
import {pages} from "./Config"


const Menu = () =>
	<div className='menu'>
		{pages.map((page) => <MenuItem page={page}/>)}
	</div>;

export default Menu
