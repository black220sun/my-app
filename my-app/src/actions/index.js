export const select = (text) => ({
	type: "PAGE_SELECTED",
	payload: text
});